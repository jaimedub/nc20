<?php
/**
 * @copyright Copyright (c) 2016 Arthur Schiwon <blizzz@arthur-schiwon.de>
 *
 * @author Arthur Schiwon <blizzz@arthur-schiwon.de>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/** @var \OCP\IL10N $l */
/** @var array $_ */

?>

<span id="activity_notifications_msg" class="msg success" style="display: none;"></span>

<?php foreach ($_['forms'] as $form) {
	if (isset($form['form'])) {?>
		<div id="<?php isset($form['anchor']) ? p($form['anchor']) : p('');?>"><?php print_unescaped($form['form']);?></div>
	<?php }
} ?>

<div class="section">
    <h2 data-anchor-name="activity"><?php p(\OC::$server->getL10N('amx_branding')->t('Hidden files')); ?></h2>
    <div id="files-setting-showhidden">
        <input class="checkbox" id="showhiddenfilesToggle" type="checkbox">
        <label for="showhiddenfilesToggle"><?php p(\OC::$server->getL10N('amx_branding')->t('Show hidden files')); ?></label>
        <input type="hidden" name="showHiddenFiles" id="showHiddenFiles" value="<?php p($_['showHiddenFiles']); ?>" />
    </div>
</div>
