#!/usr/bin/env bash

REPODIR=`git rev-parse --show-toplevel`

cd $REPODIR

# Settings
#node node_modules/handlebars/bin/handlebars -n OC.Settings.Templates  apps/settings/js/templates -f apps/settings/js/templates.js
handlebars -n OC.Settings.Templates  apps/settings/js/templates -f apps/settings/js/templates.js

# Systemtags (doesn't exist on nextcloud 20)
#node node_modules/handlebars/bin/handlebars -n OC.SystemTags.Templates core/js/systemtags/templates -f core/js/systemtags/templates.js
#handlebars -n OC.SystemTags.Templates core/js/systemtags/templates -f core/js/systemtags/templates.js

# Files app
#node node_modules/handlebars/bin/handlebars -n OCA.Files.Templates apps/files/js/templates -f apps/files/js/templates.js
handlebars -n OCA.Files.Templates apps/files/js/templates -f apps/files/js/templates.js

# Sharing
#node node_modules/handlebars/bin/handlebars -n OCA.Sharing.Templates apps/files_sharing/js/templates -f apps/files_sharing/js/templates.js
handlebars -n OCA.Sharing.Templates apps/files_sharing/js/templates -f apps/files_sharing/js/templates.js

# Files external
#node node_modules/handlebars/bin/handlebars -n OCA.Files_External.Templates apps/files_external/js/templates -f apps/files_external/js/templates.js
handlebars -n OCA.Files_External.Templates apps/files_external/js/templates -f apps/files_external/js/templates.js


#amx_branding
#Files Overwrite
handlebars -n OCA.Files.Templates apps/amx_branding/js/files/templates -f apps/amx_branding/js/files/templates.overwritten.js

#Comments
handlebars -n OCA.Files.Templates apps/amx_branding/js/comments/templates -f apps/amx_branding/js/comments/templates.overwritten.js

#Settings
handlebars -n OCA.Files.Templates apps/amx_branding/js/settings/templates -f apps/amx_branding/js/settings/templates.overwritten.js

#Sysremtags
handlebars -n OCA.Files.Templates apps/amx_branding/js/systemtags/templates -f apps/amx_branding/js/systemtags/templates.overwritten.js


if [[ $(git diff --name-only) ]]; then
    echo "Please submit your compiled handlebars templates"
    echo
    git diff
    exit 1
fi


echo "All up to date! Carry on :D"
exit 0
